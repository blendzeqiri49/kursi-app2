import React from "react";
import Cs from "classnames";
import "./Heading.css";

export const Heading = (props) => {
  const classes = Cs("Heading", {
    "Heading--center": props.centered,
    "Heading--right": props.right,
  });
  return (
    <div className={classes}>
      <h4 className="Heading__sub-title">{props.sub}</h4>
      <h2 className="Headind__main-title">{props.main}</h2>
    </div>
  );
};

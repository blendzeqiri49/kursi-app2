import React from "react";
import "./ServicesList.css";
import circleImages from "../../assets/images/Ellipse1.svg";
export const ServicesList = () => {
  const services = [
    {
      name: "web developer",
      desc:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmodtempor incididunt ut labore et dolore",
      imges: circleImages,
    },
    {
      name: "lider",
      desc:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmodtempor incididunt ut labore et dolore",
      imges: circleImages,
    },
    {
      name: "web developer",
      desc:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmodtempor incididunt ut labore et dolore",
      imges: circleImages,
    },
  ];
  return (
    <div className="ServicesList">
      {services.map((item) => {
        return (
          <div className="ServicesList__item ">
            <div className="ServicesList__images">
              <img src={item.imges} alt="circle" />
            </div>
            <h4 className="ServicesList__title">{item.name}</h4>
            <p className="ServicesList__desc">{item.desc}</p>
          </div>
        );
      })}
    </div>
  );
};

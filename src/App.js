import { Container } from "./components/Container/Container";
import { Heading } from "./components/Heading/Heading";
import { Services } from "./sections/Services/Services";


function App() {
  return (
    <div className="App">
      <Container>
     <Services/>
     <Heading sub="OUR PROTOFILE" main="Take A Look At Our Latest Work"/>
     </Container>
    </div>
  );
}

export default App;

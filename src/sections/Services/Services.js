import React from "react";
import { Heading } from "../../components/Heading/Heading";
import { ServicesList } from "../../components/ServicesList/ServicesList";
import "./Services.css";
export const Services = () => {
  return (
    <div className="Services">
      <Heading
        centered
        sub="OUR SERVICES"
        main="We Are Providing Digital services"
      />
      
      <ServicesList />
      {/* <Heading
        right
        sub="OUR SERVICES"
        main="We Are Prezoviding Digital services"
      /> */}
    </div>
  );
};
